@section('latest_posts')
    @foreach ($latestPosts as $post)
        <div class="post col-xs-12 col-sm-6 col-md-4" data-postId="{{ $post->id }}">
            <img src="{{ $post->image  }}" width="350" />
            <h4>{{ $post->title  }}</h4>
            <p>{{ $post->description  }}</p>
            <p>Created at: <b>{{ $post->created_at }}</b></p>
        </div>
    @endforeach
@endsection