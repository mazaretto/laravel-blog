<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />


        <title>Laravel / Blog</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        @include('posts.latest_posts')
        <main class="container" id="app">
            <h1>Blog - {{ config('app.name')  }}</h1>

            <section class="latest_posts">
                <h2>Latest news</h2>

                <div class="row posts">
                    @yield('latest_posts')
                </div>

                <div class="row justify-content-center">
                    <load-button v-if="buttonLoadVisible" :title="loadButtonName" :on-load="loadMore" />
                </div>
            </section>
        </main>

        <script type="text/javascript" src="{{ asset("js/app.js") }}"></script>
    </body>
</html>
