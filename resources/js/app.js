import axios from 'axios'

import LoadButton from './components/LoadButton'
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('load-button', LoadButton)

/**
 * Add posts in DOM
 * @param posts
 */
const addPosts = posts => {
    posts.forEach(post => {
        let newPostHTML = `<div class="post col-xs-12 col-sm-6 col-md-4" data-postId="${post.id}"><img src="${post.image}" width="350" /><h4>${post.title}</h4><p>${post.description}</p><p>Created at: <b>${post.created_at}</b></p></div>`

        document.querySelector('.posts').innerHTML += newPostHTML
    })
}

/**
 * Main Application Vue object.
 */
new Vue({
    el: '#app',

    data: {
        loadButtonName: "Показать ещё",
        buttonLoadVisible: true
    },

    methods: {
        /**
         * Load More posts
         * @param e
         */
        loadMore (e) {
            let getLastPostId = document.querySelectorAll('.posts > .post:last-child')
            let { postid } = getLastPostId[0].dataset

            this.loadButtonName = "Загружаю..."

            axios.get(`/posts/${postid}`).then(async response => {

                const { data } = response

                if( data && response.status === 200 ) {
                    this.loadButtonName = "Показать ещё";

                    if (data.isLastPost) this.buttonLoadVisible = false

                    addPosts(data.posts)
                }
            })
        }
    }
});
