<?php

// index route
Route::get('/', 'PostsController@index');

// route posts
Route::get('/posts/{id}', 'PostsController@getNextPostsFromId');

// route all posts
Route::get('/api/posts/{amount?}', 'PostsController@getAllPosts');

// route search posts
Route::get('/api/posts/search/{message}', 'PostsController@searchPost');

