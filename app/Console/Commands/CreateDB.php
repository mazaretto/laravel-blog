<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use PDO;

class CreateDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create default database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $host = env('DB_HOST');
        $root = env('DB_USERNAME');
        $root_password = env('DB_PASSWORD');

        $databaseName = env('DB_DATABASE');

        try {
            // PDO соединение из указанных в конфиге данных
            $dbh = new PDO("mysql:host=$host", $root, $root_password);

            // создание базы данных
            $dbh->exec('CREATE SCHEMA `' . $databaseName . '` DEFAULT CHARACTER SET utf8;')
            or die(print_r($dbh->errorInfo(), true));

            // выполнение миграции
            Artisan::call('migrate', [
                '--force' => true
            ]);

            // наполнение базы данных
            DB::table('posts')->insert([
                ['title' => "First post", "description" => "Description first post", "image" => "post_image.jpg", "created_at" => "2019-01-24 00:04:55"],
                ['title' => "Second post", "description" => "Description first post", "image" => "post_image.jpg", "created_at" => "2019-01-23 00:04:55"],
                ['title' => "Third post", "description" => "Description first post", "image" => "post_image.jpg", "created_at" => "2019-01-22 00:04:55"],
                ['title' => "Four post", "description" => "Description first post", "image" => "post_image.jpg", "created_at" => "2019-01-21 00:04:55"],
                ['title' => "Five post", "description" => "Description first post", "image" => "post_image.jpg", "created_at" => "2019-01-20 00:04:55"],
                ['title' => "Siz post", "description" => "Description first post", "image" => "post_image.jpg", "created_at" => "2019-01-19 00:04:55"],
                ['title' => "Seven post", "description" => "Description first post", "image" => "post_image.jpg", "created_at" => "2019-01-18 00:04:55"]
            ]);

            $this->info("Database has been created!");
        } catch (PDOException $e) {
            die("DB ERROR: " . $e->getMessage());
        }
    }
}
