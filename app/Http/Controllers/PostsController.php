<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Posts;
use Illuminate\Support\Facades\DB;
use Nexmo\Response;

class PostsController extends Controller
{
    /**
     * @var int
     */
    private $postsLoadCount = 3;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        $connection = mysqli_connect(env("DB_HOST"),env("DB_USERNAME"), env("DB_PASSWORD"));

        if(!mysqli_select_db($connection, env("DB_DATABASE"))) {
            return view('error',[
                'msg' => 'База данных не найдена.'
            ]);
        } else {
            $latestPosts = $this->getLatestPosts();

            return view('index', [
                'latestPosts' => $latestPosts
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function getLatestPosts ()
    {
        return Posts::limit(3)->orderBy('created_at','desc')->get();
    }

    /**
     * @param $id
     * @return false|string
     */
    public function getNextPostsFromId ($id)
    {
        $allPosts = Posts::all();
        $lastPost = $allPosts->last();
        $isLast = false;
        $postsOnLoad = [];

        // preload posts
        for($i = $id; $i < $id+($this->postsLoadCount); $i++) {
            if(isset($allPosts[$i])) {
                $post = $allPosts[$i];

                if ($post->id === $lastPost->id) $isLast = true;

                array_push($postsOnLoad, $post);
            }
        }

        return json_encode([
            "posts" => $postsOnLoad,
            "isLastPost" => $isLast
        ]);
    }

    /**
     * @param null $limit
     * @return false|string
     */
    public function getAllPosts ($limit = null)
    {
        return json_encode(Posts::limit($limit)->orderBy('created_at', 'desc')->get());
    }

    /**
     * @param $message
     * @return false|string
     */
    public function searchPost ($message)
    {
        return json_encode(Posts::limit(null)
            ->where("title", "LIKE", "%$message%")
            ->orWhere("description","LIKE","%$message%")->get());
    }
}
