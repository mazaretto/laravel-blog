#Блог на Laravel
Запуск скрипта инициализации БД:

```php
$ git clone https://mazaretto@bitbucket.org/mazaretto/laravel-blog.git
```
```php
$ cd laravel-blog
```

Файлы конфигурации:

**laravel-blog/config/database.php**

**laravel-blog/.env**

***Логин и пароль от MySQL должны быть одинаковыми в обоих файлах.**
#Создание и наполнение БД
```php
$ php artisan db:create
```
#API методы
* /api/posts/{amount}
    * amount - необязательный параметр (указывает на количество выводимых записей).
* /api/posts/search/{search_string}
    * search_string - обязательный параметр, поиск осуществляется по описанию и названию статьи.

***Ваш сервер должен поддерживать PHP 7.+, Apache-PHP-7 и MySQL**